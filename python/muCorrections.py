import os
import envyaml

from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/MUO/python/muCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class muSFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(muSFRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.wps = kwargs.pop("wps")
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.isMC:
            if not os.getenv("_muSF"):
                os.environ["_muSF"] = "_muSF"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")
                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

            if not os.getenv(f"_muSF_{self.corrKey}"):
                os.environ[f"_muSF_{self.corrKey}"] = "_muSF"

                # Declaring two objects, one for tightID and the other for tightRelIso.
                # Of course this should be changed if more WPs are needed, maybe looping over 
                # an input parameter.
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tightid_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg[self.corrKey]["fileName"],
                            corrCfg[self.corrKey]["corrNameID"]))
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_tightreliso_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg[self.corrKey]["fileName"],
                            corrCfg[self.corrKey]["corrNameIso"]))

                # alternating two different versions of the get_sf functions to comply with 
                # the two different schema of the json (i.e. w/ or w/o "year" to evaluate)

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_mu_tight_id_sf_%s(std::string syst, Vfloat eta, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 15. || fabs(eta[i]) > 2.4) sf.push_back(1.);
                            else sf.push_back(corr_tightid_%s.eval({eta[i], pt[i], syst}));
                        }
                        return sf;
                    }
                    ROOT::RVec<double> get_mu_tight_iso_sf_%s(std::string syst, Vfloat eta, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 15. || fabs(eta[i]) > 2.4) sf.push_back(1.);
                            else sf.push_back(corr_tightreliso_%s.eval({eta[i], pt[i], syst}));
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, self.corrKey, self.corrKey))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        systematics = [("", "nominal"), ("_up", "systup"), ("_down", "systdown")]
        for corr in self.wps:
            for syst_name, syst in systematics:
                # store systematics impact on SF only for the central production;
                # skip them for all systematic variated selections
                if self.skip_unused_systs and self.systs != "" and syst != "nominal":
                    continue

                df = df.Define("musf_%s%s" % (corr, syst_name),
                                'get_mu_%s_sf_%s("%s", Muon_eta, Muon_pt)' % (corr, self.corrKey, syst))

                branches.append("musf_%s%s" % (corr, syst_name))

        return df, branches


def muSFRDF(**kwargs):
    """
    Module to obtain muon SFs with their uncertainties.

    :param wps: name of the wps to consider among ``tight_id``, ``tight_iso``.
        Note: probably more can be considered.
    :type wps: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: muSFRDF
            path: Corrections.MUO.muCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
                isUL: self.dataset.has_tag('ul')
                wps: [tight_id, tight_iso]
    """
    return lambda: muSFRDFProducer(**kwargs)

